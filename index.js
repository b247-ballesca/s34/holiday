// 1. Import express using require directive
const express = require("express");

// 2. Initialize express by using it to the app variable as a function
const app = express();

// 3. Set the port number
const port = 4005;

// 4. Use middleware to allow express to read JSON
app.use(express.json());

// 5. Use middleware to allow express to be able to read more data types from a response
app.use(express.urlencoded({extended: true}));

// 6. Listen to the port and console.log a text once the server is running
app.listen(port, () => console.log(`Server is running at localhost:${port}`));


// [ SECTION ] Routes
	// Express has methods corresponding to each HTTP method


app.post("/display-name", (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

// [ SUB-SECTION ] Sign-Up Form
let users = [
	{
		"usernmae" : "brandon",
		"password" : "brandon123"
	},
	{
		"usernmae" : "john",
		"password" : "john123"
	},
];

const books = [
	{ title: 'No Logo', author: 'Naomi Klein', pages: 490 },
  	{ title: 'North', author: 'Seamus Heaney', pages: 304},
	{ title: 'To Kill a Mockingbird', author: 'Harper Lee', pages: 281 },
  	{ title: 'The Great Gatsby', author: 'F. Scott Fitzgerald', pages: 180 },
  	{ title: 'Silent Spring', author: 'Rachel Carson', pages: 400 },
  	{ title: 'Noli Me Tángere', author: 'José Rizal', pages: 438 },
  	{ title: '1984', author: 'George Orwell', pages: 328 },
  	{ title: 'The Sixth Extinction', author: 'Elizabeth Kolbert', pages: 391 },
  	{ title: 'Brave New World', author: 'Aldous Huxley', pages: 311 },
  	{ title: 'The Catcher in the Rye', author: 'J.D. Salinger', pages: 234 },
  	{ title: 'The Catcher in the Rye', author: 'J.D. Salinger', pages: 234 },

];


app.post("/sign-up", (req, res) => {
	if(req.body.username !== "" && req.body.password !== ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	} else {
		res.send("Please input BOTH username and password!");
	}
});

app.put("/delete-user", (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){

			message =`User ${req.body.username}' deleted`;

			break;
		
		} else {
			message = "User does not exits!"
		}
	}

	res.send(message);
});



app.put("/change-password", (req, res) => {
	let message;

	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			users[i].password = req.body.password;

			message =`User ${req.body.username}'s password has been updated.`;

			break;
		
		} else {
			message = "User does not exits!"
		}
	}

	res.send(message);
});

// [ACTIVITY]

// 1 
    
app.get("/books/total-pages", (request, response) => {

	let totalPages = books.map(function (books) {
    	return books.pages;
	});

    const sum = totalPages.reduce((partialSum, a) => partialSum + a, 0);
 
	response.send('The total number of pages is ' + sum);
});




